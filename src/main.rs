use std::ops::{Add, Sub};

use bevy::ecs::event::Events;
use bevy::prelude::*;

use bevy::core::FixedTimestep;
use rand::prelude::SliceRandom;
use rand::thread_rng;

// FIXME: refactor into multiple files

const BOARD_WIDTH: u32 = 10;
const BOARD_HEIGHT: u32 = 40;
const VISIBLE_HEIGHT: u32 = 20;

const TETRIMINO_POSITIONS: [[[Position; 4]; 4]; 7] = [
    // I
    [
        [
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
            Position { x: 3, y: 0 },
        ],
        [
            Position { x: 2, y: 1 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: -1 },
            Position { x: 2, y: -2 },
        ],
        [
            Position { x: 0, y: -1 },
            Position { x: 1, y: -1 },
            Position { x: 2, y: -1 },
            Position { x: 3, y: -1 },
        ],
        [
            Position { x: 1, y: 1 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: -1 },
            Position { x: 1, y: -2 },
        ],
    ],
    // J
    [
        [
            Position { x: 0, y: 1 },
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
        ],
        [
            Position { x: 1, y: -1 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 1 },
        ],
        [
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: -1 },
        ],
        [
            Position { x: 1, y: -1 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 1 },
        ],
    ],
    // L
    [
        [
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: 1 },
        ],
        [
            Position { x: 1, y: 1 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: -1 },
            Position { x: 2, y: -1 },
        ],
        [
            Position { x: 0, y: -1 },
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
        ],
        [
            Position { x: 0, y: 1 },
            Position { x: 1, y: 1 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: -1 },
        ],
    ],
    // O
    [
        [
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: 1 },
        ],
        [
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: 1 },
        ],
        [
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: 1 },
        ],
        [
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: 1 },
        ],
    ],
    // S
    [
        [
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 1 },
        ],
        [
            Position { x: 1, y: 1 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: -1 },
        ],
        [
            Position { x: 0, y: -1 },
            Position { x: 1, y: -1 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
        ],
        [
            Position { x: 0, y: 1 },
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: -1 },
        ],
    ],
    // T
    [
        [
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 0 },
        ],
        [
            Position { x: 1, y: -1 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
            Position { x: 2, y: 0 },
        ],
        [
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: -1 },
            Position { x: 2, y: 0 },
        ],
        [
            Position { x: 0, y: 0 },
            Position { x: 1, y: -1 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
        ],
    ],
    // Z
    [
        [
            Position { x: 0, y: 1 },
            Position { x: 1, y: 1 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
        ],
        [
            Position { x: 1, y: -1 },
            Position { x: 1, y: 0 },
            Position { x: 2, y: 0 },
            Position { x: 2, y: 1 },
        ],
        [
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: -1 },
            Position { x: 2, y: -1 },
        ],
        [
            Position { x: 0, y: -1 },
            Position { x: 0, y: 0 },
            Position { x: 1, y: 0 },
            Position { x: 1, y: 1 },
        ],
    ],
];

const TETRIMINO_COLORS: [Color; 7] = [
    // I
    Color::CYAN,
    // J
    Color::BLUE,
    // L
    Color::ORANGE,
    // O
    Color::YELLOW,
    // S
    Color::GREEN,
    // T
    Color::PURPLE,
    // Z
    Color::RED,
];

fn main() {
    // TODO: implement gravity timer and refactor into better architecture with smaller systems
    // that are responsible for way less (input system should NOT move anything)
    App::new()
        .insert_resource(WindowDescriptor {
            width: 200.,
            height: 400.,
            // position: Some(Vec2::new(1500., 100.)),
            title: "Tetris".to_string(),
            resizable: false,
            ..Default::default()
        })
        .insert_resource(ClearColor(Color::hex("282828").unwrap()))
        .insert_resource(Board([[None; BOARD_WIDTH as usize]; BOARD_HEIGHT as usize]))
        .insert_resource(Spawner::new())
        .init_resource::<Events<SpawnEvent>>()
        .init_resource::<Events<ClearLinesEvent>>()
        .init_resource::<Events<GameOverEvent>>()
        .add_plugins(DefaultPlugins)
        .add_startup_system(setup)
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(0.2))
                .with_system(spawn_tetrimino_event.label(State::Spawn))
                .with_system(gravity.label(State::Move).after(State::Spawn))
                .with_system(clear_lines.label(State::ClearLines).after(State::Move))
                .with_system(
                    check_game_over
                        .label(State::CheckGameOver)
                        .after(State::ClearLines),
                ),
        )
        .add_system(input)
        .add_system_set_to_stage(
            CoreStage::PostUpdate,
            SystemSet::new()
                .with_system(size_scaling)
                .with_system(position_translation),
        )
        .run();
}

#[derive(SystemLabel, PartialEq, Eq, Hash, Debug, Clone)]
enum State {
    Spawn,
    Move,
    ClearLines,
    CheckGameOver,
}

struct SpawnEvent;
struct ClearLinesEvent;
struct GameOverEvent;

struct Board([[Option<Entity>; BOARD_WIDTH as usize]; BOARD_HEIGHT as usize]);

#[derive(Component, Clone, Copy, Debug)]
struct Position {
    x: i32,
    y: i32,
}

impl Add for Position {
    type Output = Position;

    fn add(self, rhs: Self) -> Self::Output {
        Position {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Sub for Position {
    type Output = Position;

    fn sub(self, rhs: Self) -> Self::Output {
        Position {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

#[derive(Component)]
struct PixelSize {
    width: u32,
    height: u32,
}

impl PixelSize {
    fn square(x: u32) -> Self {
        Self {
            width: x,
            height: x,
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum TetriminoType {
    I,
    J,
    L,
    O,
    S,
    T,
    Z,
}

#[derive(Component)]
struct Tetrimino {
    tetrimino_type: TetriminoType,
    rotation: Rotation,
    blocks: Vec<Entity>,
}

#[derive(Clone, Copy, Debug)]
enum Rotation {
    Clockwise0,
    Clockwise90,
    Clockwise180,
    Clockwise270,
}

#[derive(Clone, Copy, Debug)]
enum RelativeRotation {
    AntiClockwise = -1,
    Clockwise = 1,
}

impl Add<RelativeRotation> for Rotation {
    type Output = Rotation;

    fn add(self, rhs: RelativeRotation) -> Self::Output {
        match self as i32 + rhs as i32 {
            0 | 4 => Rotation::Clockwise0,
            1 => Rotation::Clockwise90,
            2 => Rotation::Clockwise180,
            3 | -1 => Rotation::Clockwise270,
            _ => unreachable!(),
        }
    }
}

struct Spawner {
    types: Vec<TetriminoType>,
}

impl Spawner {
    fn new() -> Self {
        Self {
            types: random_tetrimino_order(),
        }
    }

    fn next_type(&mut self) -> TetriminoType {
        if self.types.is_empty() {
            self.types = random_tetrimino_order();
        }
        self.types.pop().unwrap()
    }

    fn reset(&mut self) {
        self.types = random_tetrimino_order();
    }
}

fn random_tetrimino_order() -> Vec<TetriminoType> {
    let mut types = vec![
        TetriminoType::I,
        TetriminoType::J,
        TetriminoType::L,
        TetriminoType::O,
        TetriminoType::S,
        TetriminoType::T,
        TetriminoType::Z,
    ];

    types.shuffle(&mut thread_rng());

    types
}

fn setup(mut commands: Commands, mut spawn_writer: EventWriter<SpawnEvent>) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    spawn_writer.send(SpawnEvent);
}

fn spawn_tetrimino_event(
    mut spawn_events: ResMut<Events<SpawnEvent>>,
    mut commands: Commands,
    game_over_writer: EventWriter<GameOverEvent>,
    board: ResMut<Board>,
    spawner: ResMut<Spawner>,
) {
    if !spawn_events.is_empty() {
        spawn_events.clear();
        spawn_tetrimino(&mut commands, game_over_writer, board, spawner);
    }
}

fn spawn_tetrimino(
    commands: &mut Commands,
    mut game_over_writer: EventWriter<GameOverEvent>,
    mut board: ResMut<Board>,
    mut spawner: ResMut<Spawner>,
) {
    let tetrimino_type = spawner.next_type();
    let rotation = Rotation::Clockwise0;

    let block_positions: Vec<_> = TETRIMINO_POSITIONS[tetrimino_type as usize][rotation as usize]
        .into_iter()
        .map(|pos| {
            pos + Position {
                x: BOARD_WIDTH as i32 / 2 - 2,
                y: VISIBLE_HEIGHT as i32 - 1,
            }
        })
        .collect();

    for pos in &block_positions {
        if board.0[pos.y as usize][pos.x as usize].is_some() {
            game_over_writer.send(GameOverEvent);
            return;
        }
    }

    let blocks = block_positions
        .iter()
        .map(|pos| {
            commands
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        color: TETRIMINO_COLORS[tetrimino_type as usize],
                        custom_size: Some(Vec2::new(10., 10.)),
                        ..Default::default()
                    },
                    ..Default::default()
                })
                .insert(*pos)
                .insert(PixelSize::square(10))
                .id()
        })
        .collect();

    let tetrimino = Tetrimino {
        tetrimino_type,
        rotation,
        blocks,
    };

    for (pos, block) in block_positions.into_iter().zip(tetrimino.blocks.iter()) {
        board.0[pos.y as usize][pos.x as usize] = Some(*block);
    }

    commands.spawn().insert(tetrimino);
}

fn gravity(
    mut commands: Commands,
    game_over_reader: EventReader<GameOverEvent>,
    mut clear_lines_writer: EventWriter<ClearLinesEvent>,
    mut spawn_writer: EventWriter<SpawnEvent>,
    mut board: ResMut<Board>,
    tetriminos: Query<(Entity, &Tetrimino)>,
    mut block_positions: Query<&mut Position>,
) {
    // skip move system if it's game over
    if !game_over_reader.is_empty() {
        return;
    }

    if let Some((tetrimino_entity, tetrimino)) = tetriminos.iter().next() {
        let can_not_move = tetrimino
            .blocks
            .iter()
            .map(|&block| block_positions.get(block).unwrap())
            .any(|&pos| {
                if pos.y == 0 {
                    return true;
                }

                if let Some(block) = board.0[pos.y as usize - 1][pos.x as usize] {
                    return !tetrimino.blocks.contains(&block);
                }

                false
            });

        if can_not_move {
            commands.entity(tetrimino_entity).despawn();
            clear_lines_writer.send(ClearLinesEvent);
            spawn_writer.send(SpawnEvent);
        } else {
            for &pos in tetrimino
                .blocks
                .iter()
                .map(|&block| block_positions.get(block).unwrap())
            {
                board.0[pos.y as usize][pos.x as usize] = None;
            }

            for &block in &tetrimino.blocks {
                let mut pos = block_positions.get_mut(block).unwrap();
                pos.y -= 1;
                board.0[pos.y as usize][pos.x as usize] = Some(block);
            }
        }
    }
}

fn check_game_over(
    mut commands: Commands,
    mut game_over_events: ResMut<Events<GameOverEvent>>,
    mut spawn_writer: EventWriter<SpawnEvent>,
    mut board: ResMut<Board>,
    mut spawner: ResMut<Spawner>,
    tetriminos: Query<Entity, With<Tetrimino>>,
    blocks: Query<Entity, With<Position>>,
) {
    if !game_over_events.is_empty() {
        game_over_events.clear();

        // despawn everything
        if let Some(tetrimino) = tetriminos.iter().next() {
            commands.entity(tetrimino).despawn();
        }

        for block in blocks.iter() {
            commands.entity(block).despawn();
        }

        // clear board
        for row in &mut board.0 {
            for elem in row {
                *elem = None;
            }
        }

        // restart
        spawner.reset();
        spawn_writer.send(SpawnEvent);
    }
}

fn size_scaling(windows: Res<Windows>, mut query: Query<(&PixelSize, &mut Transform)>) {
    let window = windows.get_primary().unwrap();
    for (size, mut transform) in query.iter_mut() {
        transform.scale = Vec3::new(
            window.width() / (BOARD_WIDTH * size.width) as f32,
            window.height() / (VISIBLE_HEIGHT * size.height) as f32,
            1.,
        );
    }
}

fn position_translation(windows: Res<Windows>, mut query: Query<(&Position, &mut Transform)>) {
    fn convert(pos: f32, bound_window: f32, bound_game: f32) -> f32 {
        let tile_size = bound_window / bound_game;
        (pos / bound_game - 0.5) * bound_window + tile_size / 2.
    }

    let window = windows.get_primary().unwrap();
    for (pos, mut transform) in query.iter_mut() {
        transform.translation = Vec3::new(
            convert(pos.x as f32, window.width(), BOARD_WIDTH as f32),
            convert(pos.y as f32, window.height(), VISIBLE_HEIGHT as f32),
            0.,
        );
    }
}

fn print_board(board: &Board) {
    for row in board.0.iter().rev() {
        for elem in row {
            print!(
                "{}",
                match elem {
                    None => '-',
                    Some(_) => '#',
                }
            );
        }
        println!();
    }
    println!();
}

#[derive(Clone, Copy)]
enum Shift {
    Left = -1,
    Right = 1,
}

enum KeyboardInput {
    Shift(Shift),
    Rotation(RelativeRotation),
    HardDrop,
}

fn input(
    game_over_reader: EventReader<GameOverEvent>,
    keyboard_input: Res<Input<KeyCode>>,
    mut board: ResMut<Board>,
    mut tetriminos: Query<&mut Tetrimino>,
    mut block_positions: Query<&mut Position>,
) {
    if !game_over_reader.is_empty() {
        return;
    }

    if let Some(mut tetrimino) = tetriminos.iter_mut().next() {
        if let Some(key) = keyboard_input.get_just_pressed().next() {
            let input = match key {
                KeyCode::Left | KeyCode::H => KeyboardInput::Shift(Shift::Left),
                KeyCode::Right | KeyCode::L => KeyboardInput::Shift(Shift::Right),
                KeyCode::Down | KeyCode::J => {
                    KeyboardInput::Rotation(RelativeRotation::AntiClockwise)
                }
                KeyCode::Up | KeyCode::K => KeyboardInput::Rotation(RelativeRotation::Clockwise),
                KeyCode::Space => KeyboardInput::HardDrop,
                _ => return,
            };

            match input {
                KeyboardInput::Shift(shift) => {
                    let can_not_move = tetrimino
                        .blocks
                        .iter()
                        .map(|&block| block_positions.get(block).unwrap())
                        .any(|pos| {
                            let new_x = pos.x + shift as i32;

                            if new_x < 0 || new_x >= BOARD_WIDTH as i32 {
                                return true;
                            }

                            if let Some(block) = board.0[pos.y as usize][new_x as usize] {
                                return !tetrimino.blocks.contains(&block);
                            }

                            false
                        });

                    if can_not_move {
                        return;
                    }

                    for &pos in tetrimino
                        .blocks
                        .iter()
                        .map(|&block| block_positions.get(block).unwrap())
                    {
                        board.0[pos.y as usize][pos.x as usize] = None;
                    }

                    for &block in &tetrimino.blocks {
                        let mut pos = block_positions.get_mut(block).unwrap();
                        pos.x += shift as i32;
                        board.0[pos.y as usize][pos.x as usize] = Some(block);
                    }
                }
                KeyboardInput::Rotation(relative_rotation) => {
                    let new_rotation = tetrimino.rotation + relative_rotation;
                    let initial_position_first_block = TETRIMINO_POSITIONS
                        [tetrimino.tetrimino_type as usize][tetrimino.rotation as usize][0];
                    let offset = *block_positions.get(tetrimino.blocks[0]).unwrap()
                        - initial_position_first_block;

                    let new_positions: Vec<Position> = TETRIMINO_POSITIONS
                        [tetrimino.tetrimino_type as usize][new_rotation as usize]
                        .iter()
                        .map(|&new_pos| new_pos + offset)
                        .collect();

                    // TODO: implement kicking
                    let can_not_rotate = new_positions.iter().any(|pos| {
                        if pos.x < 0
                            || pos.x >= BOARD_WIDTH as i32
                            || pos.y < 0
                            || pos.y >= BOARD_HEIGHT as i32
                        {
                            return true;
                        }

                        if let Some(block) = board.0[pos.y as usize][pos.x as usize] {
                            return !tetrimino.blocks.contains(&block);
                        }

                        false
                    });

                    if can_not_rotate {
                        return;
                    }

                    for &pos in tetrimino
                        .blocks
                        .iter()
                        .map(|&block| block_positions.get(block).unwrap())
                    {
                        board.0[pos.y as usize][pos.x as usize] = None;
                    }

                    for (&block, &new_pos) in tetrimino.blocks.iter().zip(new_positions.iter()) {
                        let mut pos = block_positions.get_mut(block).unwrap();
                        *pos = new_pos;
                        board.0[pos.y as usize][pos.x as usize] = Some(block);
                    }

                    tetrimino.rotation = new_rotation;

                    // TODO: implement infinity spin by resetting gravity timer
                }
                KeyboardInput::HardDrop => (),
            }
        }
    }
}

fn clear_lines(
    mut commands: Commands,
    mut clear_lines_events: ResMut<Events<ClearLinesEvent>>,
    mut board: ResMut<Board>,
    mut block_positions: Query<&mut Position>,
) {
    if clear_lines_events.is_empty() {
        return;
    }

    clear_lines_events.clear();

    let mut line = 0usize;
    'a: loop {
        while board.0[line].iter().any(Option::is_none) {
            if line as u32 == BOARD_HEIGHT - 1 {
                break 'a;
            }
            line += 1;
        }

        // clear line
        for block in board.0[line] {
            let block = block.unwrap();

            let pos = block_positions.get(block).unwrap();
            board.0[pos.y as usize][pos.x as usize] = None;

            commands.entity(block).despawn();
        }

        // move down everything above the line
        for y in line..BOARD_HEIGHT as usize - 1 {
            for x in 0..BOARD_WIDTH as usize {
                board.0[y][x] = board.0[y + 1][x].take();
            }

            for block in board.0[y] {
                if let Some(block) = block {
                    let mut pos = block_positions.get_mut(block).unwrap();
                    pos.y -= 1;
                }
            }
        }
    }
}
